package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC001Login extends ProjectMethods{
	
	@Test(dataProvider="dt")
	public void CreateLead(String fname, String lname, String cname, String email, String ph) {
		
		WebElement createlead = locateElement("LinkText","Create Lead");
		click(createlead);
		
		WebElement firstname = locateElement("createLeadForm_firstName");
		type(firstname, fname);
		WebElement lasname = locateElement("createLeadForm_lastName");
		type(lasname, lname);
		WebElement fieldcompanyname = locateElement("createLeadForm_companyName");
		type(fieldcompanyname, cname);
		
		WebElement fieldemail = locateElement("createLeadForm_primaryEmail");
		type(fieldemail, email);
		
		WebElement fieldph = locateElement("createLeadForm_primaryPhoneNumber");
		type(fieldph, ph);
		
		WebElement sourcefield = locateElement("createLeadForm_dataSourceId");
		selectDropDownUsingIndex(sourcefield, 5);
		WebElement datefield = locateElement("createLeadForm_birthDate");
		type(datefield, "10/2/2000");
		WebElement submitbutton = locateElement("class", "smallSubmit");
		click(submitbutton);
		
	}
	
}












