package testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.databind.deser.impl.ExternalTypeHandler;


public class LearnReports {
	@Test
	public void report() throws IOException {
		
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest logger = extent.createTest("TC001Login", "Create Lead");
		logger.assignAuthor("Revathy");
		logger.assignCategory("Smoke");
		logger.log(Status.PASS, "The Username entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
		logger.log(Status.PASS, "The Password entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build());
		logger.log(Status.PASS, "The login button clicked successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img3.png").build());
		extent.flush();
	}
}
