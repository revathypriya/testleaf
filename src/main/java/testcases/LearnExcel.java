package testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static Object[][] data(String dataSheetName) throws IOException {
		
		
		XSSFWorkbook wb = new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);
		
		int lastRowNum = sheet.getLastRowNum();
		short lastCellNum = sheet.getRow(0).getLastCellNum();
		Object[][] data = new Object[lastRowNum][lastCellNum];
		for (int i = 1; i <= lastRowNum; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < lastCellNum; j++) {
				XSSFCell cell = row.getCell(j);
				
				try {
					String cellvalue = cell.getStringCellValue();
					data[i-1][j]=cellvalue;
					System.out.println(cellvalue);
				} catch (NullPointerException e) {
					System.out.println("");
				}
			}
		}
		return data;
		
	}

}
