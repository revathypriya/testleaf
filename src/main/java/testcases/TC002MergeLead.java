package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002MergeLead extends ProjectMethods{
	
	
	@Test
	public void MergeLead() {
	
		WebElement createlead = locateElement("LinkText","Create Lead");
		click(createlead);
		WebElement fieldcompanyname = locateElement("createLeadForm_companyName");
		type(fieldcompanyname, "CG");
		WebElement firstname = locateElement("createLeadForm_firstName");
		type(firstname,"Revathy");
		WebElement lasname = locateElement("createLeadForm_lastName");
		type(lasname, "Athi");
		WebElement sourcefield = locateElement("createLeadForm_dataSourceId");
		selectDropDownUsingIndex(sourcefield, 5);
		WebElement datefield = locateElement("createLeadForm_birthDate");
		type(datefield, "10/2/2000");
		WebElement submitbutton = locateElement("class", "smallSubmit");
		click(submitbutton);
		WebElement mergeleads = locateElement("LinkText", "Merge Leads");
		click(mergeleads);
		WebElement fromleads = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(fromleads);
		switchToWindow(1);
	}
	
}
