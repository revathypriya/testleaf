package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLeadpage;
import pages.HomePage;
import pages.LoginPage;
import pages.MyHome;
import pages.MyLeadPage;
import wdMethods.ProjectMethods;

public class TC001_Login extends ProjectMethods{

@BeforeTest
public void setData() {
	testCaseName = "TC001_Login";
	testDesc = "Login to application";
	category = "Regression";	
	author= "Revathy & Mathi";
	dataSheetName = "LoginCredentials";
	
}
	
	@Test(dataProvider="dt")
	public void login(String uName, String uPassword,String cName,String fName,String lName) {
		
		new LoginPage()
		.enterusername(uName)
		.enterpassword(uPassword)
		.clicklogin();
		
		new HomePage()
		.linkcrmsfa();
		
		new MyHome()
		.clickLeads();
		
		new MyLeadPage()
		.clickcreatelead();
		
		new CreateLeadpage()
		.companyName(cName)
		.firstName(fName)
		.lastName(lName)
		.createLeadbtn();
			
	}
}
