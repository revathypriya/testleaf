package week4.day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrameAlert {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		// Invoke chrome driver class
		ChromeDriver driver = new ChromeDriver();
		
		//to maximize the screen
		driver.manage().window().maximize();
		
		// open the url
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[contains(text(),'Try it')]").click();
		Alert alert = driver.switchTo().alert();
		alert.sendKeys("Revathy");
		Thread.sleep(3000);
		alert.accept();
		String text = driver.findElementByXPath("//p[@id=\"demo\"]").getText();
		boolean check = text.contains("Revathy11");
		if (check==true) {
			System.out.println("Verified the text");
			}else
				System.out.println("Displayed text is wrong");

		
	}

}
