package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.Get;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		// Invoke chrome driver class
		ChromeDriver driver = new ChromeDriver();
		
		//to maximize the screen
		driver.manage().window().maximize();
		
		// open the url
		driver.get("https://www.irctc.co.in/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("(//span[contains(text(),'AGENT LOGIN')])[1]").click();
		driver.findElementByLinkText("Contact Us").click();
		
		Set<String> allwindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allwindows);
		driver.switchTo().window(lst.get(0));
		driver.switchTo().window(lst.get(1));
		System.out.println(driver.getTitle());
		
		File srcfile = driver.getScreenshotAs(OutputType.FILE);
		File obj = new File("./Snaps/img1.jpeg");
		FileUtils.copyFile(srcfile, obj);
		
		String secondwindow = driver.getWindowHandle();
		for (String ewind:allwindows) {
			if (ewind.compareTo(secondwindow)==0)
			{
				driver.close();

		}
	}

	}}
