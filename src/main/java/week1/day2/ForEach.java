package week1.day2;

import java.util.Scanner;

public class ForEach {

	public static void main(String[] args) {
		Scanner Scan = new Scanner(System.in);
		int sum = 0;
		System.out.println("Enter the size:");
		int size = Scan.nextInt();
		int array[] = new int[size];
		for (int i=0; i<size; i++) {
			array[i] = Scan.nextInt();
		}
for (int i : array) {
	if (i%2 != 0)
	{
		sum = sum+i;
	}
		
}
System.out.println(sum);
	}

}
