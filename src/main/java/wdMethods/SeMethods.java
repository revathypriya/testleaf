package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import utils.Report;

public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public static RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {	
				/*ChromeOptions op = new ChromeOptions();
				op.addArguments("--disable-notifications");
				driver=new ChromeDriver(op);*/
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.out.println("The Browser "+browser+" not Launched ");
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "LinkText":return driver.findElementByLinkText(locValue);
			case "PartialLinkText":return driver.findElementByPartialLinkText(locValue);
			}
		} catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
		}return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
		ele.clear();
		ele.sendKeys(data);
		//reportStep(Status.PASS, "The data"+data+"clicked successfully");
		System.out.println("The Data "+data+" is Entered Successfully");
		}
		catch (WebDriverException e) {
		//	reportStep(Status.FAIL, "The data"+data+"clicked successfully");
		}
		finally {
		takeSnap();
	}}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" Clicked Successfully");
		takeSnap();
	}
	public void clickWithoutSnap(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" Clicked Successfully");
	}
	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			System.out.println("The DropDown Is Selected with "+value);
		} catch (ElementNotVisibleException e) {
			System.out.println("The given value is not available in the dropdown ");
		}finally{
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd1 = new Select(ele);
			dd1.selectByIndex(index);
			System.out.println("The DropDown Is Selected with "+index);
		} catch (ElementNotVisibleException e) {
			System.out.println("The given value is not available in the dropdown ");
		}finally{
			takeSnap();
		}

	}

	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select dd2 = new Select(ele);
			dd2.selectByValue(value);
			System.out.println("The DropDown Is Selected with "+value);
		} catch (ElementNotVisibleException e) {
			System.out.println("The given value is not available in the dropdown ");
		}finally{
			takeSnap();
		}

	}
	@Override
	public boolean verifyTitle(String expectedTitle) {
		try {
		String title = driver.getTitle();
		if (expectedTitle==title) {
			System.out.println("The expected title is "+title );
			return true;
		}}catch (Exception e) {
			// TODO: handle exception
			System.out.println("Tiltle is not populated ");
		}
		finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
		String text = ele.getText();
		if(text==expectedText) {
			System.out.println("Text is same");
		}
		}
		catch (java.util.NoSuchElementException e) {
			System.out.println("No Text is found");
		}
		finally {
			takeSnap();
		}
		
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			if(text.contains(expectedText)){
				System.out.println("Text is same");
			}
				
		}
			catch (NoSuchElementException e) {
				System.out.println("No Text is found");
			}
			finally {
				takeSnap();
			}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override 
	public void verifySelected(WebElement ele) {
		try {
		if(ele.isSelected()){
			System.out.println("True");
			
		}}catch (NoSuchElementException e) {
			System.out.println("No Text is found");
		}
		finally {
			takeSnap();
		}
			

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
		WebDriver frame = driver.switchTo().frame(ele);
		
		}catch (NoSuchElementException e) {
			System.out.println("Frame is not found");
		}
		finally {
			takeSnap();
		}
		

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
	}

	@Override
	public String getAlertText() {
		driver.switchTo().alert().getText();
		return null;
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();

	}

}
