package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import testcases.LearnExcel;


public class ProjectMethods extends SeMethods{
	public String dataSheetName;
	@BeforeMethod
	public void login() {
	startApp("chrome", "http://leaftaps.com/opentaps");
	/*WebElement eleUserName = locateElement("id", "username");
	type(eleUserName, "DemoSalesManager");
	WebElement elePassword = locateElement("password");
	type(elePassword, "crmsfa");
	WebElement eleLogin = locateElement("class","decorativeSubmit");
	click(eleLogin);
	WebElement crmsfalink = locateElement("LinkText", "CRM/SFA");
	click(crmsfalink);*/
}
	@AfterMethod
	/*public void closeapp(){
		closeAllBrowsers();
	}*/
	
	@DataProvider(name="dt")
	public Object[][] testdata() throws IOException{
		LearnExcel dp = new LearnExcel();
		Object[][] data= dp.data(dataSheetName);
		return data;
		
	}
	
	
}
