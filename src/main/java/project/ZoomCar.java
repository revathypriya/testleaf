package project;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class ZoomCar extends SeMethods{

	@Test
	public void zoomcarapp() {
	startApp("chrome","https://www.zoomcar.com/chennai/");
	WebElement title = locateElement("xpath","//a[@class='search']");
	title.click();
	WebElement pickuppoints = locateElement("xpath","(//div[@class='items'])[2]");
	pickuppoints.click();
	WebElement next1 = locateElement("class", "proceed");
	next1.click();
	WebElement pickdate = locateElement("xpath","//div[@class='days']/child::div[2]");
	pickdate.click();
	WebElement next2 = locateElement("class", "proceed");
	next2.click();
	WebElement pickeddate = locateElement("xpath","//div[@class='days']/child::div[1]");
	verifySelected(pickeddate);
	WebElement next3 = locateElement("class", "proceed");
	next3.click();
	List<WebElement> pricelist = driver.findElementsByClassName("price");
	List<WebElement> prlst = new ArrayList<>();
	prlst.addAll(pricelist);
	System.out.println(prlst.size());
	int max =0;
	for (WebElement webElement : prlst) {
		String s = webElement.getText().substring(2);
		
		if(max<Integer.parseInt(s)) {
			max=Integer.parseInt(s);
			
		}
	
		}System.out.println("The greater value is " + max);
		String bn = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/../../preceding::h3[1]").getText();
		System.out.println("Name of the car is "+bn);
		driver.findElementByXPath("//div[contains(text(),'"+max+"')]/following-sibling::button").click();
		
			
	}	
}

