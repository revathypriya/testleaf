package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadPage extends ProjectMethods {
	public MyLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.LINK_TEXT,using="Create Lead") WebElement createlead;
	
	public CreateLeadpage clickcreatelead() {
		click(createlead);
		return new CreateLeadpage();
		
	}
	
	

}
