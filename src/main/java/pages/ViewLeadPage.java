package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.LINK_TEXT,using="Edit") WebElement eleEditbtn;
	@FindBy(how=How.LINK_TEXT,using="Delete") WebElement eledeletebtn;
	
	public ViewLeadPage clickEditbtn() {
		click(eleEditbtn);
		return this;
	}
	
	public MyLeadPage clickDeletebtn() {
		click(eledeletebtn);
		return new MyLeadPage();
	}
}
