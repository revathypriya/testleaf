package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadpage extends ProjectMethods{

	public CreateLeadpage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleCreateLeadbtn;
	
	public CreateLeadpage companyName(String data) {
		type(eleCompanyName,data);
		return this;
	}
	public CreateLeadpage firstName(String data) {
		type(eleFirstName,data);
		return this;
	}
	public CreateLeadpage lastName(String data) {
		type(eleLastName,data);
		return this;
	}
	public ViewLeadPage createLeadbtn() {
		click(eleCreateLeadbtn);
		return new ViewLeadPage();
	}
}
