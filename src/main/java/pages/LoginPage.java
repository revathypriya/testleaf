package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods{

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID,using="username") WebElement eleusername;
	@FindBy(how=How.ID,using="password") WebElement elepassword;
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit") WebElement elelogin;
	
	public LoginPage enterusername(String data) {
	type(eleusername, data);
	return this;
	}
	

	public LoginPage enterpassword(String data1) {
	type(elepassword, data1);
	return this;
	}
	

	public HomePage clicklogin() {
	click(elelogin);
	return new HomePage();
	}
	
	
	
	
}
