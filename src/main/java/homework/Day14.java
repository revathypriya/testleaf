package homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Day14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sum = 0;
		List<Integer> lst = new ArrayList<>();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the number");
		int num = scan.nextInt();
		for (int i = 1; i < num; i++) {
			if(i%3==0 || i%5==0) {
				sum = sum+i;
				lst.add(i);
			}
			
		}
		System.out.println(lst);
		
		System.out.println("sum of all the number is " + sum);
	}

}
