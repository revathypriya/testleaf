package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTables {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		//to invoke the Chrome driver
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				
				// Invoke chrome driver class
				ChromeDriver driver = new ChromeDriver();
				
				//to maximize the screen
				driver.manage().window().maximize();
				
				// open the url
				driver.get("https://erail.in");
				driver.findElementById("txtStationFrom").clear();
				driver.findElementById("txtStationFrom").sendKeys("TBM",Keys.TAB);
				driver.findElementById("txtStationTo").clear();
				driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
				Thread.sleep(2000);
				driver.findElementById("chkSelectDateOnly").click();
				Thread.sleep(2000);
				WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
				List<WebElement> allrows = table.findElements(By.tagName("tr"));
				System.out.println(allrows.size());
				for (WebElement eachrow : allrows) {
					System.out.println(eachrow.findElements(By.tagName("td")).get(1).getText());
				}
				
	}

}
