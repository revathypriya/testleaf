package week3.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IRCTC {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		// Invoke chrome driver class
		ChromeDriver driver = new ChromeDriver();
		
		//to maximize the screen
		driver.manage().window().maximize();
		
		// open the url
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		List<WebElement> linktext  = driver.findElementsByTagName("a");
		//linktext.size();
		System.out.println(linktext.size());
		linktext.get(3).click();
		driver.findElementById("userRegistrationForm:userName").sendKeys("Rev_123");
		driver.findElementById("userRegistrationForm:password").sendKeys("Train@123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Train@123");
		WebElement elementById = driver.findElementById("userRegistrationForm:securityQ");
		Select dr = new Select(elementById);
		dr.selectByVisibleText("Who was your Childhood hero?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Vijay");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Revathy");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Priya");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Athi");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement birthdate = driver.findElementById("userRegistrationForm:dobDay");
		Select bthdate = new Select(birthdate);
		bthdate.selectByIndex(8);
		
		WebElement birthmonth = driver.findElementById("userRegistrationForm:dobMonth");
		Select bthmonth = new Select(birthmonth);
		bthmonth.selectByValue("06");
		
		WebElement birthyear = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select bthyear = new Select(birthyear);
		bthyear.selectByVisibleText("1988");
		
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select occt = new Select(occupation);
		occt.selectByIndex(3);
		
		driver.findElementById("userRegistrationForm:uidno").sendKeys("DF33F2223");
		driver.findElementById("userRegistrationForm:idno").sendKeys("ABCDE1234K");
		
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select county = new Select(country);
		county.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:email").sendKeys("test@123");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9876543215");
		
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select nation = new Select(nationality);
		nation.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("f-14, ak apatment, tric street");
		driver.findElementById("userRegistrationForm:street").sendKeys("12 Street");
		driver.findElementById("userRegistrationForm:area").sendKeys("Sengan");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("625014",Keys.TAB);
		Thread.sleep(5000);
		
		WebElement cityName = driver.findElementById("userRegistrationForm:cityName");
		Select ctName = new Select(cityName);
		ctName.selectByIndex(1);
		Thread.sleep(5000);
		WebElement postOffice = driver.findElementById("userRegistrationForm:postofficeName");
		Select postoff = new Select(postOffice);
		postoff.selectByIndex(8);
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("042589456");
		driver.findElementById("userRegistrationForm:resAndOff:1").click();
		Thread.sleep(5000);
		
		driver.findElementById("userRegistrationForm:addresso").sendKeys("111, trest");
		driver.findElementById("userRegistrationForm:streeto").sendKeys("Test1 street");
		driver.findElementById("userRegistrationForm:areao").sendKeys("Test2 Street");
		WebElement country2 = driver.findElementById("userRegistrationForm:countrieso");
		Select c2 = new Select(country2);
		c2.selectByValue("94");
		
		driver.findElementById("userRegistrationForm:pincodeo").sendKeys("603103",Keys.TAB);
		Thread.sleep(5000);
		WebElement cityname2 = driver.findElementById("userRegistrationForm:cityNameo");
		Select ctyname2 = new Select(cityname2);
		ctyname2.selectByIndex(1);
		Thread.sleep(5000);
		
		WebElement postoffice2 = driver.findElementById("userRegistrationForm:postofficeNameo");
		Select postoff2 = new Select(postoffice2);
		postoff2.selectByValue("Thaiyur B.O");
		
		driver.findElementById("userRegistrationForm:landlineo").sendKeys("8975642315");
		
		
		
	}

}
