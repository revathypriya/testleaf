package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LearnLogin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        
		//to invoke the Chrome driver
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		// Invoke chrome driver class
		ChromeDriver driver = new ChromeDriver();
		
		//to maximize the screen
		driver.manage().window().maximize();
		
		// open the url
		driver.get("http://leaftaps.com/opentaps/");
		
		// login
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		
		driver.findElementById("createLeadForm_companyName").sendKeys("Capgemini");
		driver.findElementById("createLeadForm_firstName").sendKeys("Revathy");
		driver.findElementById("createLeadForm_lastName").sendKeys("Athi");
		//driver.findElementByClassName("smallSubmit").click();
		
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(source);
		dd.selectByVisibleText("Public Relations");
		
		WebElement source1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dd1 = new Select(source1);
		dd1.selectByValue("CATRQ_AUTOMOBILE");
		
		WebElement source2 = driver.findElementById("createLeadForm_industryEnumId");
		Select dd2 = new Select(source2);
		
		List<WebElement> options = dd2.getOptions();
		
		for (WebElement opt : options) {
			String s = opt.getText();
			if(s.startsWith("M")) {
			System.out.println(s);
			}
			
			
		}
		}
		
	}

